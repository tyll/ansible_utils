from setuptools import setup, Command
from ansible_utils import utils_version


class PyTest(Command):
    user_options = []
    def initialize_options(self):
        pass
    def finalize_options(self):
        pass
    def run(self):
        import subprocess
        errno = subprocess.call(['py.test', 'tests'])
        raise SystemExit(errno)

setup(name='ansible_utils',
      version=utils_version,
      description='support tools for working with ansible playbooks',
      author='Tim Flink',
      author_email='tflink@fedoraproject.org',
      license='GPLv3+',
      url='https://bitbucket.org/tflink/ansible_utils',
      packages=['ansible_utils'],
      package_dir={'ansible_utils': 'ansible_utils'},
      entry_points=dict(console_scripts=['rbac-playbook=ansible_utils.rbac_playbook:main']),
      include_package_data=True,
      cmdclass={'test': PyTest},
      install_requires=[
          'ansible>=1.6',
          'kitchen>=1.1.1'
      ]
)

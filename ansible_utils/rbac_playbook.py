#!/usr/bin/python2
# -*- coding: utf-8 -*-
# rbac-playbook: wrapper script for controlling execution of ansible playbooks
#                through group membershp
#
# Primary maintainer: Tim Flink <tflink@fedoraproject.org>
#
# Copyright 2014, Red Hat, Inc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""
rbac-playbook tool for controlling execution of ansible-playbooks via POSIX
group membership
"""

import argparse
import re
import pwd
import grp
from hashlib import sha256
import sys
import smtplib
import socket
from email.mime.text import MIMEText
import os
import yaml
import copy

from pprint import pprint

from kitchen.text.converters import to_bytes


CONF_DIRS = ['/etc/ansible_utils']

CONF_FILE = 'rbac.yaml'

DEFAULT_CONFIG = {'config': {'NOTIFY_ADDRESS': 'root@localhost.localdomain',
                             'SMTP_HOST': 'localhost',
                             'PLAYBOOKS_DIR': '/srv/ansible/',
                             'ANSIBLE_PLAYBOOK': '/usr/bin/ansible-playbook',
                             'EMAIL_ENABLED': True,
                             'DEBUG_MODE': False
                             },
                  'acls': {}
                  }


def get_config():
    """ Retrieve configuration from yaml file and merge with default settings
    in case not everything has been defined

    :return: dictionary of configuration values
    """
    config_values = copy.deepcopy(DEFAULT_CONFIG)
    new_settings = read_configfile()

    for config_item in new_settings['config']:
        new_item = new_settings['config'][config_item]
        config_values['config'].update({config_item: new_item})

    if 'acls' in new_settings:
        config_values['acls'] = new_settings['acls']

    return config_values


def read_configfile():
    """ Read configuation yaml file from one of the configured directories

    :return: parsed yaml file
    """
    for conf_dir in CONF_DIRS:
        acl_filename = "{0}/{1}".format(conf_dir, CONF_FILE)
        if os.path.exists(acl_filename):
            with open(acl_filename, 'r') as acl_file:
                return yaml.safe_load(acl_file.read())
    return {'config': []}


config = get_config()


class RbacException(Exception):
    pass

class RbacAuthException(RbacException):
    pass


def notify(message):
    """ send notification of activity. Will use mail if MAIL_ENABLED is set to
    true and DEBUG_MODE is set to false (default)

    :param message: dictionary with 'subject' and 'body' keys representing
                    message to send
    """
    if not config['config']['EMAIL_ENABLED'] or config['config']['DEBUG_MODE']:
        print "NOTIFY: %s" % message['subject']
        pprint(message['body'])
    else:
        email_notify(message)


def email_notify(message):
    """ send an email notification of activity
    :param message: dictionary with 'subject' and 'body' keys containing message
                    to send via email
    """
    s = smtplib.SMTP(config['config']['SMTP_HOST'])
    msg = MIMEText(to_bytes(message['body']))
    msg['Subject'] = to_bytes(message['subject'])
    msg['From'] = to_bytes('root@{0}'.format(socket.getfqdn()))
    msg['To'] = config['config']['NOTIFY_ADDRESS']
    s.sendmail(msg['From'], [msg['To']], to_bytes(msg.as_string()))
    s.quit()


def generate_message(result, username, playbook_name, args, checksum):
    """ Generate a message used to indicate rbac-ansible activity

    :param result: SUCCESS/FAILURE depending on whether the action was authorized
    :param username: name of user requesting action
    :param playbook_name: playbook attempted
    :param args: args given to rbac
    :param checksum: checksum of playbook which was accessed
    :return: rendered string summarizing rbac activity
    """
    subject = "[rbac-playbook] {0} {1} ran {2}".format(result,
                                                       username,
                                                       playbook_name)
    body = ['Details:']
    body.extend(['{0}: {1}'.format(key, value) for key, value in args.iteritems()])
    body.append("Sha256: {0}".format(checksum))
    return {'subject': subject, 'body': '  \n'.join(body)}


def get_checksum(filename):
    """ computes a sha256 checksum of a specified file

    :param filename: filename to get checksum of
    :return: sha512 of the file
    """
    full_filename = os.path.abspath(os.path.join(config['config']['PLAYBOOKS_DIR'],
                                                 filename))

    # Not handling any errors: in case there is an error determining, we just bail out
    with open(full_filename, 'r') as fd:
        filecontents = fd.read()
        return sha256(filecontents).hexdigest()


def process_args(args):
    """ some arguments need to be split into a list before they can be properly
    used. Split them but ignore others

    :param args: raw arguments
    :return: arguments including those which have been split
    """
    # the only args we want to split up are limit and tags
    for key in args:
        if key in ['limit', 'tags']:
            if args[key] is not None and isinstance(args[key], basestring):
                args[key] = re.split('[\,\;\:]', args[key])

    return args


def run_sudo_command(playbook_file, playbook_args):
    """ run the ansible-playbook using sudo via os.execv

    :param playbook_file: playbook to execute
    :param playbook_args: args that should be passed to ansible-playbook
    """
    full_filename = os.path.abspath(os.path.join(config['config']['PLAYBOOKS_DIR'],
                                                 playbook_file))

    python_args = ['cd', config['config']['ANSIBLE_DIR'], ';',
                   '/usr/bin/python2', config['config']['ANSIBLE_PLAYBOOK'],
                   full_filename]
    python_args.extend(playbook_args)
    executable = '/usr/bin/sudo'
    args = ['-i', '/bin/bash', '-i', '-c',
            ' '.join(python_args)]

    # Note: (1) The function used here has to pass the environment to
    # ansible-playbook so that it can connect to the ssh-agent correctly
    # (2) subprocess.call([...]shell=True) is one way to do (1) but it
    # is all sorts of insecure.  We'd have to manually go through and
    # escape all shell special chars to do that correctly.  Use execv
    # instead

    if config['config']['DEBUG_MODE']:
        print "EXECV: %s %s" % (executable, ' '.join(args))
    else:
        print "EXECV: %s %s" % (executable, ' '.join(args))
        os.execv(executable, args)

def _get_username():
    """Retrieve the username for the user which started execution of rbac_playbook"""

    user = os.getlogin()
    username = pwd.getpwnam(user)
    return username.pw_name

def _get_group_members(groupname):
    """Find the members of a specific group

    :param groupname: name of group to find members of
    :return: list of usernames for members of the given group, empty list if the group does not exist"""

    group_data = None
    try:
        group_data = grp.getgrnam(groupname)
    except KeyError:
        return []

    return group_data.gr_mem

def _get_playbook_authorized_users(grouplist):
    """Retrieve a set of all users who are members of one or more groups

    :param grouplist: list of one or more group names
    :return: set of usernames corresponding to the union of members for each group in the grouplist"""

    userlist = []
    for groupname in grouplist:
        userlist.extend(_get_group_members(groupname))

    return set(userlist)

def can_run(acl, username, playbook_file):
    """ determines whether the current user is allowed to run a playbook

    :param acl: dictionary of acl information
    :param username: username of user running the utility
    :param playbook_file: playbook file that is being run
    :return: True if the user is authorized, False if unauthorized or the
             playbook filename is not in the acl information
    """

    try:
        authorized_groups = acl[playbook_file]
    except KeyError:
        return False

    if playbook_file in acl:
        pb_authorized = _get_playbook_authorized_users(authorized_groups)
        if username in pb_authorized:
            return True
    return False

def generate_args(options):
    """ Generate ansible-playbook compatible args representing the information
    passed into rbac
    :param options: dict of internally-represented options
    :return: list of arguments meant for ansible-playbook
    """

    playbook_args = []
    if 'limit' in options and options['limit'] is not None:
        limit_arg = ':'.join(options['limit'])
        playbook_args.extend(['-l', limit_arg])

    if 'tags' in options and options['tags'] is not None:
        tags_arg = ','.join(options['tags'])
        playbook_args.extend(['-t', tags_arg])

    if 'user' in options and options['user'] is not None:
        playbook_args.extend(['-u', options['user']])

    if 'check' in options and options['check']:
        playbook_args.append('--check')

    if 'start_at_task' in options and options['start_at_task'] is not None:
        playbook_args.append('--start-at-task=\"{0}\"'.format(options['start_at_task']))

    return playbook_args


def rbac_playbook(playbook_name, options):
    """ Role Based Access Control for running ansible-playbook. Determines whether
    the current user is authorized to run a given playbook. If authorized, render
    options for ansible-playbook and execute using sudo.

    :param playbook_name: name of playbook file to run
    :param options: dictionary of options
    """
    username = _get_username()
    checksum = get_checksum(playbook_name)

    # raise exception if not allowed
    if not can_run(config['acls'], username, playbook_name):
        notify(generate_message('FAILURE', username, playbook_name, options, checksum))
        msg ="user {0} is not authorized to run {1}".format(username, playbook_name)
        raise RbacAuthException(msg)


    # make sure there are no wildcards in the playbook
    if '*' in playbook_name:
        notify(generate_message('FAILURE', username, playbook_name, options, checksum))
        raise RbacException("Wildcards are not allowed in the playbook filename.")

    # if we're to this point, auth and sanity checking are good
    notify(generate_message('SUCCESS', username, playbook_name, options, checksum))

    # run command
    playbook_args = generate_args(options)
    run_sudo_command(playbook_name, playbook_args)


def get_argparser():
    """
    :return: confgured argparse object for processing rbac arguments from the
    command line
    """
    parser = argparse.ArgumentParser(prog="rbac")
    parser.add_argument("playbook", help="playbook to use")
    parser.add_argument("-l", "--limit", help="Hostnames to limit ansible to")
    parser.add_argument("-C", "--check", default=False, action='store_true',
                        help="don't make any changes; instead, try to predict some of the changes that may occur")
    parser.add_argument("-t", "--tags", help="only run plays and tasks tagged with these values")
    parser.add_argument("-u", "--user", help="connect as this user")
    parser.add_argument("--start-at-task", help="start the playbook at the task matching this name")
    return parser


def main():
    """ Get arguments and information from the command line describing the playbook
    that is to be run by the current user. If the user is authorized to run said
    playbook, execute it using sudo and report activities via email (if enabled)
    """
    parser = get_argparser()
    args = parser.parse_args()

    options = vars(args)

    if config['config']['DEBUG_MODE']:
        print "config:"
        pprint(config)
        print "args:"
        pprint(args)

    playbook_name = options.pop('playbook')
    options = process_args(options)

    # the idea here is that errors in rbac execution will generate exceptions
    # but a failure in the playbook execution after auth will send back a retcode
    # instead of an exception
    try:
        rbac_playbook(playbook_name, options)
    except RbacAuthException, e:
        print e
        sys.exit(1)


if __name__ == '__main__':
    main()
